import React from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity,
    FlatList
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import api_data from './youtube/data.json';
import VideoItem from './app/components/video_item';

export default class App extends React.Component {
    render() {
        return (
            <View style={style.container}>
                <View style={style.navBar}>
                    <Image style={style.imgLogo} source={require('./app/image/logo.png')} />
                    <View style={style.rightNav}>
                        <TouchableOpacity>
                            <Icon name="search" size={25} style={style.navItem} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Icon name="account-circle" size={25} style={style.navItem} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={style.bodyContent}>
                    <FlatList 
                    data={api_data.items}
                    renderItem={ (video) => <VideoItem video={video.item} /> }
                    keyExtractor={ (item) => item.id }
                    ItemSeparatorComponent={ ()=> <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}/>
                </View>

                <View style={style.tabBar}>
                    <TouchableOpacity style={style.tabItem}>
                        <Icon name="home" size={25} />
                        <Text style={style.tabTitle}>Home</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.tabItem}>
                        <Icon name="whatshot" size={25} />
                        <Text style={style.tabTitle}>Trending</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.tabItem}>
                        <Icon name="subscriptions" size={25} />
                        <Text style={style.tabTitle}>Subscriptions</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.tabItem}>
                        <Icon name="folder" size={25} />
                        <Text style={style.tabTitle}>Library</Text>
                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}


const style = StyleSheet.create({
    container : {
        flex: 1,
    },

    bodyContent : {
        flex: 1,
    },


    imgLogo: {
        width: 99, 
        height: 22
    },

    navBar : {
        height: 55,
        backgroundColor: 'white',
        elevation: 1,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    rightNav : { 
        flexDirection: 'row'
    },

    navItem : {
        marginLeft: 25,
    },

    tabBar : {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#e7e7e7',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },

    tabItem : {
        alignItems: 'center',
        justifyContent: 'center',
    },

    tabTitle : {
        fontSize: 11,
        color: '#3c3c3c',
        paddingTop: 4,
    }

});