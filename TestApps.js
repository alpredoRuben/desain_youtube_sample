import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component {
  render() {
    return (
      <View style={ style.container}>
        <View style={ style.navbar}>
          <Image source={ require('./app/image/logo.png') } style={{ width: 99, height: 22}} />
          <View style={ style.rigthnav }>
            <TouchableOpacity>
              <Icon style={ style.navitem } name="serach" size={25} />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon style={ style.navitem } name="account-circle" size={25} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
container : {
  flex: 1,
},

  navbar: {
    height:55,
    backgroundColor: 'white',
    elevation: 1,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  rigthnav: {
    flexDirection: 'row'
  },

  navitem : {
    marginLeft: 25,
  }
});

