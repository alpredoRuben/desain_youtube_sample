import React from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

function nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + ' views';
  }
  

export default class VideoItems extends React.Component { 
    
    render() {
        let data_video = this.props.video;

        return (
            
            <View style={style.container}>
                <Image source={{ uri: data_video.snippet.thumbnails.medium.url }} style={style.imgThumbnail} />
                <View style={style.imgDescription}>
                    <Image source={{ uri: 'https://randomuser.me/api/portraits/men/0.jpg'}} style={style.imgUser} />
                    <View style={style.videoDetails}>
                        <Text numberOfLines={2} style={style.videoTitleDetails}>{ data_video.snippet.title }</Text>
                        <Text style={style.videoStatus}>
                            { data_video.snippet.channelTitle + ' · ' + nFormatter(data_video.statistics.viewCount, 1) + ' · 3 months ago' }
                        </Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name="more-vert" size={20} color="#888888"/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const style = StyleSheet.create({
    container : {
        padding: 15,
    },

    imgThumbnail : {
        height : 200
    },

    imgDescription : {
        flexDirection: 'row',
        paddingTop: 15,
    },

    imgUser : {
        width: 50, 
        height: 50,
        borderRadius: 25,
    },

    videoDetails : {
        paddingHorizontal: 15,
        flex: 1
    },

    videoTitleDetails : {
        fontSize: 16,
        color: '#3c3c3c' 
    },

    videoStatus : {
        fontSize: 14,
        paddingTop: 3
    }
});